class Utils { 
    static createRectBitmapData(game, width, height, fillStyle, strokeStyle, strokeWeight = 0) {
        var bmd = game.add.bitmapData(width, height);
        var halfStroke = strokeWeight / 2;
        return Utils.drawShape(bmd, (bmd) => bmd.ctx.rect(halfStroke,halfStroke,width-strokeWeight,height-strokeWeight), game, fillStyle, strokeStyle, strokeWeight);
    }

    static createCircleBitmapData(game, radius, fillStyle, strokeStyle, strokeWeight = 0) {
        var bmd = game.add.bitmapData(2*radius, 2*radius);
        return Utils.drawShape(bmd, (bmd) => bmd.ctx.arc(radius,radius,radius - strokeWeight,0,2*Math.PI), game, fillStyle, strokeStyle, strokeWeight);
    }

    static drawShape(bmd, shapeCallback, game, fillStyle, strokeStyle, strokeWeight) {
        bmd.ctx.beginPath();
        shapeCallback(bmd);
        return Utils.draw(bmd, fillStyle, strokeStyle,strokeWeight);
    }

    static draw(bmd, fillStyle, strokeStyle, strokeWeight) {
        if (fillStyle) {
            bmd.ctx.fillStyle = fillStyle;
            bmd.ctx.fill();
        }
        if (strokeStyle || strokeWeight) {
            if (strokeStyle) bmd.ctx.strokeStyle = strokeStyle;
            if (strokeWeight) bmd.ctx.lineWidth = strokeWeight;
            bmd.ctx.stroke();
        }
        return bmd;
    }
}