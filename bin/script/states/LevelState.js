function LevelState(game, levelData) {

    this.preload = function() {
        commandSequencer = new CommandSequencer(game);
        commandSequencer.preload();
    }

    this.create = function() {
        //Create objects
        gridManager = new GridManager(game, levelData.grid);
        botManager = new BotManager(game, levelData.bots);
        executor = new Executor();
        commandBuilder = new CommandBuilder(game);
        endEvaluator = new EndEvaluator(levelData.minimumSteps);
        levelView = new LevelView(game, levelData);

        //Inject dependencies
        botManager.gridManager = gridManager;
        botManager.commandBuilder = commandBuilder;
        botManager.commandSequencer = commandSequencer;

        executor.commandBuilder = commandBuilder;
        executor.botManager = botManager;
        executor.resetDelegate = resetGrid;
        executor.commandSequencer = commandSequencer;
        executor.endEvaluator = endEvaluator;

        commandBuilder.gridManager = gridManager;
        commandBuilder.executor = executor;
        commandBuilder.botManager = botManager;

        commandSequencer.botManager = botManager;
        commandSequencer.commandBuilder = commandBuilder;
        commandSequencer.executor = executor;

        endEvaluator.botManager = botManager;
        endEvaluator.executor = executor;

        levelView.gridManager = gridManager;
        levelView.botManager = botManager;

        //Initialize
        botManager.init();
        commandSequencer.init();
        levelView.assembleLevelContent();
        levelView.fitLevelToView();

        //Inject late for lazy layering
        summaryView = new SummaryView(game, levelData);
        endEvaluator.summaryView = summaryView;
        
        introView = new IntroView(game, levelData);
        introView.present();
    }

    this.update = function() {
        executor.update();
    }
    
    this.shutdown = function() {
        bitmapDataCollection.forEach((bmd) => {
            bmd.destroy();
        });
        gridManager.destroy();
        botManager.destroy();
        commandSequencer.destroy();
        summaryView.destroy();
        introView.destroy();
    }

    function resetGrid() {
        botManager.revertBotsToInitialState();
    }

    var bitmapDataCollection = [];
    var gridManager;
    var botManager;
    var commandBuilder;
    var executor;
    var commandSequencer;
    var endEvaluator;
    var levelView;
    var summaryView;
    var introView;
}

var devLevelData = {
    levelNumber: 1,
    levelName: "Pinwheel",
    minimumSteps: 8,
    grid: {
        tileSize: 80,
        layout: [
            [ 1, 1, 1, 1, 1, 1, 1, 1 ],
            [ 1, 0, 0, 2, 0, 0, 0, 1 ],
            [ 1, 0, 0, 0, 0, 0, 0, 1 ],
            [ 1, 0, 0, 0, 0, 0, 2, 1 ],
            [ 1, 2, 0, 0, 0, 0, 0, 1 ],
            [ 1, 0, 0, 0, 0, 0, 0, 1 ],
            [ 1, 0, 0, 0, 2, 0, 0, 1 ],
            [ 1, 1, 1, 1, 1, 1, 1, 1 ]
        ]
    },
    bots: [
        // {
        //     gridPosition: {x: 2, y:3}, 
        //     commands: [
        //         {
        //             type: "move",
        //             x: 1,
        //             y: 0
        //         },
        //     ], 
        //     energy: 5, 
        //     maxEnergy: 5
        // },
        {
            gridPosition: {x: 3, y:3}, 
            commands: [
                {
                    type: "move",
                    x: 0,
                    y: -1
                },
                {
                    type: "move",
                    x: 0,
                    y: -1
                },
                {
                    type: "move",
                    x: 0,
                    y: 1
                },
                {
                    type: "move",
                    x: 0,
                    y: 1
                }
            ], 
            energy: 5, 
            maxEnergy: 5
        },
        {
            gridPosition: {x: 3, y:4}, 
            commands: [
                {
                    type: "move",
                    x: -1,
                    y: 0
                },
                {
                    type: "move",
                    x: -1,
                    y: 0
                },
                {
                    type: "move",
                    x: 1,
                    y: 0
                },
                {
                    type: "move",
                    x: 1,
                    y: 0
                }
            ], 
            energy: 5, 
            maxEnergy: 5
        },
        {
            gridPosition: {x: 4, y:4}, 
            commands: [
                {
                    type: "move",
                    x: 0,
                    y: 1
                },
                {
                    type: "move",
                    x: 0,
                    y: 1
                },
                {
                    type: "move",
                    x: 0,
                    y: -1
                },
                {
                    type: "move",
                    x: 0,
                    y: -1
                }
            ], 
            energy: 5, 
            maxEnergy: 5
        },
        {
            gridPosition: {x: 4, y:3}, 
            commands: [
                {
                    type: "move",
                    x: 1,
                    y: 0
                },
                {
                    type: "move",
                    x: 1,
                    y: 0
                },
                {
                    type: "move",
                    x: -1,
                    y: 0
                },
                {
                    type: "move",
                    x: -1,
                    y: 0
                }
            ], 
            energy: 5, 
            maxEnergy: 5
        },
    ]
};