function EndEvaluator(stepsToWin) {
    this.evaluateEndConditions = function() {
        if (!evaluationComplete) {
            var allBotsAreAlive = true;
            var allBotsAreDead = true;
            this.botManager.bots.forEach((bot) => {
                if (bot.getEnergy() <= 0) {
                    allBotsAreAlive = false;
                } else {
                    allBotsAreDead = false;
                }
            });
            var minStepsToWinHaveElapsed = this.executor.currentStep === stepsToWin - 1;
            if (allBotsAreDead || (minStepsToWinHaveElapsed && !allBotsAreAlive)) {
                levelFailed();
            } else if (allBotsAreAlive && minStepsToWinHaveElapsed) {
                levelWon();
            }
        }
    };

    this.reset = function() {
        evaluationComplete = false;
    }

    function levelFailed() {
        console.log("not win :(");
        that.executor.pause();
        that.summaryView.present(false, getCommandCount(), that.executor.attempts);
        evaluationComplete = true;
    }

    function levelWon() {
        console.log("win!");
        that.summaryView.present(true, getCommandCount(), that.executor.attempts);
        evaluationComplete = true;
    }

    function getCommandCount() {
        var commandCount = 0;
        that.botManager.bots.forEach((bot) => {
            commandCount += bot.commands.length;
        });
        return commandCount;
    }

    this.botManager;
    this.executor;
    this.summaryView;

    var that = this;
    var evaluationComplete = false;
}