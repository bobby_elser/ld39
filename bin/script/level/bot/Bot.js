function Bot(game, sprite, botData, viewSize) {
    this.getCommandIndexForStep = function(step) {
        var commandIndex = step % this.commands.length;  //automatically loop back to first command
        //if (lastGotoIndex != undefined) //select command based on the subset of commands between and including the last goto and its target
        return commandIndex;
    }

    this.finishCurrentCommand = function(newCommand) {
        if (this.currentCommand) {
            this.currentCommand.end();
        }
        this.RechargeThisStep = false;
        this.isDefinitelyLeavingTile = false;
    };

    this.reset = function() {
        this.gridPosition = botData.gridPosition;
        this.setEnergy(botData.energy);
        this.maxEnergy = botData.maxEnergy;
        this.setHoldingBattery(false);
        this.RechargeThisStep = false;
        this.isDefinitelyLeavingTile = false;
        this.currentCommand = undefined;
    };

    this.getEnergy = function() {
        return energy;
    }

    this.setEnergy = function(newEnergy) {
        energy = newEnergy;
        energyText.text = `${energy}/${that.maxEnergy}`;
    }

    this.getHoldingBattery = function() {
        return isHoldingABattery;
    }

    this.setHoldingBattery = function(nowHoldingBattery) {
        isHoldingABattery = nowHoldingBattery;
        batteryIndicator.visible = nowHoldingBattery;
    };

    this.showHighlight = function () {
        highlight.visible = true;
    };

    this.hideHighlight = function () {
        highlight.visible = false;
    };

    function init() {
        that.view = game.add.group();
        that.view.x = sprite.x;
        that.view.y = sprite.y;
        that.view.add(sprite);
        sprite.x = 0;
        sprite.y = 0;

        var textStyle = {fontSize:16};
        energyText = game.add.text( -viewSize/2 + 2, -viewSize/2 + 2, "*/*", textStyle, that.view);
        that.view.add(energyText);
        that.setEnergy(botData.energy);

        var batteryIndicatorBMD = Utils.createCircleBitmapData(game, 8, "#0ff");
        bitmapDataCollection.push(batteryIndicatorBMD);
        batteryIndicator = that.view.create(viewSize/2 - 20, -viewSize/2 + 4, batteryIndicatorBMD);
        batteryIndicator.visible = false;

        var highlightBMD = Utils.createCircleBitmapData(game, sprite.width/2 + 4, null, "white", 3);
        bitmapDataCollection.push(highlightBMD);
        highlight = that.view.create(0,0,highlightBMD);
        highlight.anchor.x = 0.5;
        highlight.anchor.y = 0.5;
        highlight.visible = false;
    }

    this.view;
    this.gridPosition;
    this.maxEnergy = botData.maxEnergy;
    this.RechargeThisStep = false;
    this.tileContentType = TileContentType.Bot;
    this.isDefinitelyLeavingTile = false;

    this.commands = [];
    this.currentCommand;
    
    var that = this;
    var bitmapDataCollection = [];
    var energy;
    var energyText;
    var isHoldingABattery = false;
    var batteryIndicator;
    var highlight;
    //var lastGotoIndex;

    init();

    this.destroy = function() {
        bitmapDataCollection.forEach((bmd) => {
            bmd.destroy();
        });
    }
}