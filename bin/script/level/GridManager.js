function GridManager(game, gridData) {
    this.getWorldPositionOfTileCenter = function(tileX, tileY) {
        return {
            x: (tileX + 0.5) * this.TileSize,
            y: (tileY + 0.5) * this.TileSize
        };
    };

    function buildTile(worldX, worldY, tileType) {
        var bmdIndex;
        switch (tileType) {
            case TileTypes.Wall: 
                bmdIndex = wallBMDIndex; break;
            case TileTypes.BatterySpawn: 
                bmdIndex = batterySpawnBMDIndex; break;
            case TileTypes.Floor:
            default:
                bmdIndex = floorBMDIndex;
        }
        return game.add.sprite(worldX, worldY, bitmapDataCollection[bmdIndex]);
    }
    
    function init() {
        floorBMDIndex = bitmapDataCollection.push(Utils.createRectBitmapData(game, gridData.tileSize, gridData.tileSize, 'gray', 'black', 1)) - 1;
        wallBMDIndex = bitmapDataCollection.push(Utils.createRectBitmapData(game, gridData.tileSize, gridData.tileSize, 'white', 'black', 1)) - 1;
        batterySpawnBMDIndex = bitmapDataCollection.push(Utils.createRectBitmapData(game, gridData.tileSize, gridData.tileSize, 'blue', 'black', 1)) - 1;

        that.tileSprites = game.add.group();
        that.Tiles = new Array(that.LayoutData.length);
        
        for (var y=0; y<that.LayoutData.length; y++) {
            var layoutRow = that.LayoutData[y];
            var row = new Array(layoutRow.length);
            for (var x=0; x<layoutRow.length; x++) {
                var worldX = x * that.TileSize;
                var worldY = y * that.TileSize;
                var tileType = layoutRow[x];
                var tileSprite = that.tileSprites.add(buildTile(worldX, worldY, tileType));
                row[x] = new Tile(tileType, tileSprite, x, y);
            }
            that.Tiles[y] = row;
        };
    }

    this.LayoutData = gridData.layout;
    this.TileSize = gridData.tileSize;
    this.Tiles;
    this.tileSprites;

    var that = this;
    var bitmapDataCollection = [];
    var floorBMDIndex;
    var wallBMDIndex;
    var batterySpawnBMDIndex;

    init();

    this.destroy = function() {
        bitmapDataCollection.forEach((bmd) => {
            bmd.destroy();
        });
    }
}

var TileTypes = {
    Floor: 0,
    Wall: 1,
    BatterySpawn: 2
};

var Tile = function(type, sprite, x, y) {
    this.type = type;
    this.sprite = sprite;
    this.gridPosition = {x:x, y:y};
    this.content = null;
    this.numberOfBotsAboutToEnter = 0;
};

var TileContentType = {
    Bot: "bot",
    Battery: "battery"
};