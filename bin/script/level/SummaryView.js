function SummaryView(game, levelData) {
    this.present = function(didWin, totalCommands, totalRetries) {
        resultTextView.text = (didWin) ? `Level ${levelData.levelNumber}\n"${levelData.levelName}"\nSuccess!` : "That didn't seem to\nwork very well. Try a\ndifferent approach!";
        stayButtonText.text = (didWin) ? "Stay and improve" : "Keep trying";
        commandTextView.text = `Your solution used ${totalCommands} commands. More is better!`;
        retryTextView.text = `It took you ${totalRetries} attempts. Less is better!`;
    
        if (!didWin) {
            continueButtonGroup.visible = false;
            stayButtonGroup.x += stayButton.width/2 + 20;
        } else {
            continueButtonGroup.visible = true;
            stayButtonGroup.x = game.camera.width/2 - 20;
        }

        reveal();
    };

    this.hide = function() {
        var tween = game.add.tween(viewGroup).to({alpha: 0}, 1500)
        tween.onComplete.add(finishHiding);
        tween.start();
    };

    function reveal() {
        viewGroup.visible = true;
        viewGroup.alpha = 0;
        game.add.tween(viewGroup).to({alpha: 1}, 1500).start();
    }

    function finishHiding() {
        viewGroup.alpha = 0;
        viewGroup.visible = false;
    }

    function returnToLevel() {
        that.hide();
        var escKey = game.input.keyboard.addKey(Phaser.Keyboard.ESC);
        escKey.onDown.add(restoreView);
    }

    function restoreView() {
        reveal();
    }

    function loadNextLevel() {
        var nextLevelId = levelData.nextLevelId;
        game.state.start(nextLevelId);
    }

    var that = this;
    var bitmapDataCollection = [];
    var viewGroup = game.add.group();
    var overlayBMD = Utils.createRectBitmapData(game, game.camera.width, game.camera.height, "black");
    var overlay = viewGroup.create(0,0,overlayBMD);
    var resultTextView = game.add.text(game.camera.width/2, game.camera.height/8, "Something's not right...", {fill: 'white', fontSize: 64, align: 'center'});
    var commandTextView = game.add.text(game.camera.width/2, 0.55 * game.camera.height, "You shouldn't see this screen yet!", {fill: 'white'});
    var retryTextView = game.add.text(game.camera.width/2, 0.65 * game.camera.height, "You're either cheating or really, really lost...", {fill: 'white'});
    var buttonBMD = Utils.createRectBitmapData(game, 300, 50, "white");
    var stayButtonGroup = game.add.group();
    var stayButton = game.add.button(0, 0, buttonBMD, returnToLevel);
    var stayButtonText = game.add.text(-stayButton.width/2, stayButton.height/2, "STOP CHEATING");
    var continueButtonGroup = game.add.group();
    var continueButton = game.add.button(0, 0, buttonBMD, loadNextLevel);
    var continueButtonText = game.add.text(continueButton.x + continueButton.width/2, continueButton.y + continueButton.height/2, "Next Level");
    
    commandTextView.anchor.x = 0.5;
    resultTextView.anchor.x = 0.5;
    retryTextView.anchor.x = 0.5;

    stayButtonGroup.x = game.camera.width/2 - 20;
    stayButtonGroup.y = 0.75 * game.camera.height;
    stayButtonGroup.add(stayButton);
    stayButtonGroup.add(stayButtonText);
    stayButton.anchor.x = 1.0;
    stayButtonText.anchor.x = 0.5;
    stayButtonText.anchor.y = 0.5;
    
    continueButtonGroup.x = game.camera.width/2 + 20;
    continueButtonGroup.y = 0.75 * game.camera.height;
    continueButtonGroup.add(continueButton);
    continueButtonGroup.add(continueButtonText);
    continueButtonText.anchor.x = 0.5;
    continueButtonText.anchor.y = 0.5;

    viewGroup.add(commandTextView);
    viewGroup.add(resultTextView);
    viewGroup.add(retryTextView);
    viewGroup.add(stayButtonGroup);
    viewGroup.add(continueButtonGroup);

    viewGroup.visible = false;
    viewGroup.alpha = 0;

    overlay.alpha = 0.8;
    overlay.inputEnabled = true;
    bitmapDataCollection.push(overlayBMD);
    bitmapDataCollection.push(buttonBMD);

    this.destroy = function() {
        bitmapDataCollection.forEach((bmd) => {
            bmd.destroy();
        });
    }
}