var MoveCommand = function(bot, moveX, moveY, gridManager, game, executor, botManager) {
    this.prepareToChooseAnimation = function() {
        startGridPosition = bot.gridPosition;
        startWorldPosition = gridManager.getWorldPositionOfTileCenter(startGridPosition.x, startGridPosition.y);
        targetTile = gridManager.Tiles[startGridPosition.y + that.y][startGridPosition.x + that.x];
        targetTile.numberOfBotsAboutToEnter += 1;
        
        var tileType = targetTile.type;
        targetTileIsImpassable = tileType === TileTypes.Wall || tileType === TileTypes.BatterySpawn;
        
        targetTileContainsStuff = false;
        botManager.bots.forEach((otherBot) => {
            var otherBotIsOnTargetTile = otherBot.gridPosition.x === targetTile.gridPosition.x && otherBot.gridPosition.y === targetTile.gridPosition.y;
            if (!targetTileContainsStuff && otherBotIsOnTargetTile) {
                targetTileContainsStuff = true;
                botAtTarget = otherBot;
                if (bot.getHoldingBattery()) {
                    otherBot.RechargeThisStep = true;
                    bot.setHoldingBattery(false);
                }
            }
        });
    };

    this.animate = function() {
        initForTargetTileContent();
        if (bot.RechargeThisStep) {
            bot.setEnergy(bot.maxEnergy);
        } else {
            bot.setEnergy(Math.max(0, bot.getEnergy() - that.energyCost));
        }
        tween.start();
    };

    this.end = function() {
        if (tween) {
            tween.stop(true);
        }
    };

    function initForTargetTileContent() {
        var multipleBotsMovingIntoTargetTile = targetTile.numberOfBotsAboutToEnter > 1;
        willBounceOffTarget = targetTileIsImpassable || targetTileContainsStuff || multipleBotsMovingIntoTargetTile;
        if (targetTile.type === TileTypes.BatterySpawn) {
            bot.setHoldingBattery(true);
            //TODO: deplete battery spawn and start cooldown?
        }
        
        var animationDuration = executor.stepDurationMillis / executor.playbackSpeed;
        if (botAtTarget) {
            //Will that bot leave? Might be making the same check on another bot, or even on this one. Follow the chain.
        }
        if (willBounceOffTarget) {
            //bounce off target tile
            endGridPosition = startGridPosition;
            endWorldPosition = startWorldPosition;
            var bounceDisplacement = {
                x: (targetTile.gridPosition.x != startGridPosition.x) ? (gridManager.TileSize - bot.view.width) / 2 : 0, 
                y: (targetTile.gridPosition.y != startGridPosition.y) ? (gridManager.TileSize - bot.view.height) / 2 : 0
            };
            if (targetTile.gridPosition.x < startGridPosition.x) bounceDisplacement.x = -bounceDisplacement.x;
            if (targetTile.gridPosition.y < startGridPosition.y) bounceDisplacement.y = -bounceDisplacement.y;
            tween = game.add.tween(bot.view).to({x: [startWorldPosition.x + bounceDisplacement.x, endWorldPosition.x],
                                                 y: [startWorldPosition.y + bounceDisplacement.y, endWorldPosition.y]},
                                                 animationDuration,
                                                 Phaser.Easing.Quintic.Out);
        } else {
            //move to target tile
            endGridPosition = {x: targetTile.gridPosition.x, y: targetTile.gridPosition.y};
            endWorldPosition = gridManager.getWorldPositionOfTileCenter(targetTile.gridPosition.x, targetTile.gridPosition.y);
            tween = game.add.tween(bot.view).to({x: endWorldPosition.x, y: endWorldPosition.y}, animationDuration, Phaser.Easing.Quintic.Out);
        }

        tween.onComplete.add(animationFinished);
    }

    function animationFinished() {
        bot.gridPosition = endGridPosition;
        bot.view.position = endWorldPosition;
        tween = undefined;
        botToBeBumpedInto = undefined;
        targetTile.numberOfBotsAboutToEnter = 0;
    }

    this.energyCost = 1;
    this.type = CommandTypes.Move;
    this.x = moveX;
    this.y = moveY;

    var that = this;
    var startGridPosition;
    var startWorldPosition;
    var targetTile;
    var endGridPosition;
    var endWorldPosition;
    var tween;
    var willBounceOffTarget;
    var targetTileContainsStuff;
    var targetTileIsImpassable;
    var botAtTarget;
}