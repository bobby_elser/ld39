function SequencerSlot(x,y,sequencer) {
    this.select = function() {
        if (sequencer.canEdit) {
            sequencer.selectSlot(that);
        }
    };

    this.setSprite = function(newSprite) {
        newSprite.anchor.x = 0.5;
        newSprite.anchor.y = 0.5;
        newSprite.x = that.button.x;
        newSprite.y = that.button.y;
        that.commandSprite = newSprite;
    };

    this.x = x;
    this.y = y;
    this.button;
    this.commandSprite;
    this.commandIndex;

    var that = this;
}