function CommandSequencer(game) {

    this.preload = function() {
        game.load.image('arrow-left-button', 'img/arrow-left.png'); //40x40
        game.load.image('arrow-up-button', 'img/arrow-up.png'); //40x40
        game.load.image('arrow-right-button', 'img/arrow-right.png'); //40x40
        game.load.image('arrow-down-button', 'img/arrow-down.png'); //40x40
        game.load.image('wait-button', 'img/wait.png'); //40x40
        game.load.image('add-command-button', 'img/add-command.png'); //34x34
        game.load.image('remove-command-button', 'img/remove-command.png'); //34x34
        game.load.image('play-button', 'img/play-button.png'); //80x80
        game.load.image('return-edit-button', 'img/return-edit-button.png'); //80x80

        var panelBMD = Utils.createRectBitmapData(game, that.width, that.height, "#ccc");
        panelBMDIndex = bitmapDataCollection.push(panelBMD) - 1;
        
        var slotBMD = Utils.createRectBitmapData(game, slotSize, slotSize, "#333");
        slotBMDIndex = bitmapDataCollection.push(slotBMD) - 1;

        var highlighterSize = slotSize + 4;
        var slotHighlighterBMD = Utils.createRectBitmapData(game, highlighterSize, highlighterSize, null, "#c00", 3);
        slotHighlighterBMDIndex = bitmapDataCollection.push(slotHighlighterBMD) - 1;
    }
    
    this.init = function() {
        var panelSprite = game.add.sprite(0, 0, bitmapDataCollection[panelBMDIndex]);

        returnEditButton = game.add.button(20, 30, 'return-edit-button', beginEditMode);
        returnEditButton.visible = false;

        buildSlots();
        buildSequenceButtons();

        var playButton = game.add.button(that.width - 95, 0, 'play-button', beginExecutionMode);
        playButton.y = (that.height - playButton.height) / 2;

        guiGroup = game.add.group();
        guiGroup.add(panelSprite);
        guiGroup.add(returnEditButton);
        guiGroup.add(slotGroup);
        guiGroup.add(buttonGroup);
        guiGroup.add(playButton);
        guiGroup.y = game.camera.height - that.height;

        that.selectBot(that.botManager.bots[0]);
        that.selectSlot(slotRows[0][0]);
    }

    this.selectBot = function(newBot) {
        if (newBot != selectedBot) {
            firstSlotCommandIndex = 0;
            if (selectedBot) selectedBot.hideHighlight();
            selectedBot = newBot;
            selectedBot.showHighlight();
            refreshSequenceDisplay();
            var commandIndexToSelectByDefault = selectedBot.commands.length;
            if (selectedBot.commands.length <= 1) {
                commandIndexToSelectByDefault = 0;
            }
            this.selectSlot(getSlotForCommandIndex(commandIndexToSelectByDefault));
        }
    };

    this.selectSlotForStep = function(step) {
        this.selectSlot(getSlotForCommandIndex(selectedBot.getCommandIndexForStep(step)));
    };

    this.selectSlot = function(slot) {
        selectedSlot = slot;
        slotHighlighter.x = slot.button.x;
        slotHighlighter.y = slot.button.y;
    };

    this.showNextPage = function() {

    };

    this.showPreviousPage = function() {

    };

    this.shiftFromSelected = function() {
        if (that.canEdit && selectedSlot.commandIndex < selectedBot.commands.length) {
            selectedBot.commands.splice(selectedSlot.commandIndex, 0, that.commandBuilder.build({type:CommandTypes.Wait}, selectedBot));
            that.selectSlot(getSlotForCommandIndex(selectedSlot.commandIndex + 1));
            refreshSequenceDisplay();
        } else {
            trySetCommandForSelectedSlot({type:CommandTypes.Wait});
        }
    };

    this.removeSelected = function() {
        if (that.canEdit && selectedSlot.commandIndex <= selectedBot.commands.length) {
            if (selectedBot.commands.length === 1) {
                that.setSelectedSlotToWait();
                that.selectSlot(slotRows[0][0]);
            } else {
                selectedBot.commands.splice(selectedSlot.commandIndex, 1);
                that.selectSlot(getSlotForCommandIndex(selectedSlot.commandIndex - 1));
                refreshSequenceDisplay();
            }
        }
    }

    this.setSelectedSlotToMoveUp = function() {
        trySetCommandForSelectedSlot({type:CommandTypes.Move, x:0, y:-1});
    };

    this.setSelectedSlotToMoveRight = function() {
        trySetCommandForSelectedSlot({type:CommandTypes.Move, x:1, y:0});
    };

    this.setSelectedSlotToMoveDown = function() {
        trySetCommandForSelectedSlot({type:CommandTypes.Move, x:0, y:1});
    };

    this.setSelectedSlotToMoveLeft = function() {
        trySetCommandForSelectedSlot({type:CommandTypes.Move, x:-1, y:0});
    };

    this.setSelectedSlotToWait = function() {
        trySetCommandForSelectedSlot({type:CommandTypes.Wait});
    };

    function clearSequenceFromSlots() {
        slotRows.forEach((slotRow) => {
            slotRow.forEach((slot) => {
                if (slot.commandSprite) {
                    slot.commandSprite.destroy();
                }
            });
        });
    }

    function drawSelectedSequence() {
        var commandIndex = firstSlotCommandIndex;
        for (var y=0; y<slotRows.length; y++) {
            for (var x=0; x<slotRows[y].length; x++) {
                var slot = slotRows[y][x];
                slot.commandIndex = commandIndex;
                if (commandIndex < selectedBot.commands.length) {
                    var command = selectedBot.commands[commandIndex];
                    switch(command.type) {
                        case CommandTypes.Move:
                            if (command.x === 0) {
                                if (command.y > 0) { // moving down
                                    slot.setSprite(slotGroup.add(game.add.sprite(slot.button.x, slot.button.y, 'arrow-down-button')));
                                } else if (command.y < 0) { // moving up
                                    slot.setSprite(slotGroup.add(game.add.sprite(slot.button.x, slot.button.y, 'arrow-up-button')));
                                } else {
                                    //Both command.y and command.x are zero, command should be a wait and not a move
                                    console.error("Move command has zero movement, should be wait");
                                }
                            } else if (command.y === 0) {
                                if (command.x > 0) { // moving right
                                    slot.setSprite(slotGroup.add(game.add.sprite(slot.button.x, slot.button.y, 'arrow-right-button')));
                                } else if (command.x < 0) { // moving left
                                    slot.setSprite(slotGroup.add(game.add.sprite(slot.button.x, slot.button.y, 'arrow-left-button')));
                                } else {
                                    //Both command.y and command.x are zero, command should be a wait and not a move
                                    console.error("Move command has zero movement, should be wait");
                                }
                            } else {
                                //Both command.y and command.x are nonzero, making this diagonal movement which is unsupported.
                                console.error("Diagonal movement not supported, no appropriate command graphic exists.");
                            }
                            break;
                        case CommandTypes.Wait:
                            slot.setSprite(slotGroup.add(game.add.sprite(slot.button.x, slot.button.y, 'wait-button')));
                            break;
                        default:
                    }
                }
                commandIndex++;
            }
        }
    }

    function buildSlots() {
        var slotRowCount = 2;
        var slotCountPerRow = 10;
        slotGroup = game.add.group();
        slotRows = new Array(2);
        for (var y=0; y<slotRowCount; y++) {
            slotRows[y] = new Array(slotCountPerRow);
            var slotRow = slotRows[y];
            for (var x=0; x<slotCountPerRow; x++) {
                var slotCenterX = slotSpacing + (x * (slotSize + slotSpacing)) + slotSize/2;
                var slotCenterY = slotSpacing + (y * (slotSize + slotSpacing)) + slotSize/2;
                var slot = new SequencerSlot(x, y, that);
                slotRow[x] = slot;
                var slotButton = game.add.button(slotCenterX, slotCenterY, bitmapDataCollection[slotBMDIndex], slot.select);
                slotButton.anchor.x = 0.5;
                slotButton.anchor.y = 0.5;
                slot.button = slotButton;
                slotGroup.add(slotButton);
            }
        }
        slotHighlighter = slotGroup.create(0,0,bitmapDataCollection[slotHighlighterBMDIndex]);
        slotHighlighter.anchor.x = 0.5;
        slotHighlighter.anchor.y = 0.5;

        slotGroup.x = 130;
        slotGroup.y = (that.height - (slotSpacing + slotRowCount * (slotSize + slotSpacing))) / 2;
    }

    function buildSequenceButtons() {
        buttonGroup = game.add.group();
        var upButton = buttonGroup.add(game.add.button(40, 0, 'arrow-up-button', that.setSelectedSlotToMoveUp));
        var rightButton = buttonGroup.add(game.add.button(80, 40, 'arrow-right-button', that.setSelectedSlotToMoveRight));
        var downButton = buttonGroup.add(game.add.button(40, 80, 'arrow-down-button', that.setSelectedSlotToMoveDown));
        var leftButton = buttonGroup.add(game.add.button(0, 40, 'arrow-left-button', that.setSelectedSlotToMoveLeft));
        var waitButton = buttonGroup.add(game.add.button(40, 40, 'wait-button', that.setSelectedSlotToWait));
        var addButton = buttonGroup.add(game.add.button(3, 3, 'add-command-button', that.shiftFromSelected));
        var removeButton = buttonGroup.add(game.add.button(83,3, 'remove-command-button', that.removeSelected));
        buttonGroup.x = 10; // that.width - 120;
        buttonGroup.y = (that.height - 120) / 2;
    }

    function refreshSequenceDisplay() {
        clearSequenceFromSlots();
        drawSelectedSequence();
    }

    function trySetCommandForSelectedSlot(commandData) {
        if (that.canEdit) {
            if (selectedSlot.commandIndex >= selectedBot.commands.length) {
                var extraWaits = new Array(selectedSlot.commandIndex - selectedBot.commands.length + 1);
                for (var i=0; i<extraWaits.length; i++)
                    extraWaits[i] = that.commandBuilder.build({type:CommandTypes.Wait}, selectedBot);
                selectedBot.commands = selectedBot.commands.concat(extraWaits);
            }
            selectedBot.commands[selectedSlot.commandIndex] = that.commandBuilder.build(commandData, selectedBot);
            that.selectSlot(getSlotForCommandIndex(selectedSlot.commandIndex + 1));
            refreshSequenceDisplay();
        }
    }

    function getSlotForCommandIndex(commandIndex) {
        if (commandIndex < firstSlotCommandIndex) {
            //Command index is before the first slot, so just return the first slot.
            return slotRows[0][0];
        }
        var y=0;
        var slotCount = firstSlotCommandIndex;
        while (y < slotRows.length && slotCount + slotRows[y].length <= commandIndex) {
            slotCount += slotRows[y].length;
            y++;
        }
        if (y < slotRows.length) {
            return slotRows[y][commandIndex - slotCount];
        } else {
            //Command index is beyond the last slot, so just return the last slot.
            var lastRow = slotRows[slotRows.length-1];
            return lastRow[lastRow.length-1];
        }
    }

    function beginExecutionMode() {
        that.canEdit = false;
        buttonGroup.visible = false;
        returnEditButton.visible = true;
        //that.executor.stop();
        that.executor.attempts++;
        that.executor.startExecutionMode();
    }

    function beginEditMode() {
        that.executor.stop();
        returnEditButton.visible = false;
        buttonGroup.visible = true;
        that.canEdit = true;
    }

    this.botManager;
    this.commandBuilder;
    this.executor;
    this.height = 140;
    this.width = game.camera.width;
    this.canEdit = true;

    var that = this;
    var guiGroup;
    var buttonGroup;
    var slotGroup;
    var bitmapDataCollection = [];
    var panelBMDIndex;
    var slotBMDIndex;
    var slotRows;
    var selectedSlot;
    var selectedBot;
    var firstSlotCommandIndex = 0;
    var slotSize = 44;
    var slotSpacing = 12;
    var slotHighlighter;
    var returnEditButton;

    this.destroy = function() {
        bitmapDataCollection.forEach((bmd) => {
            bmd.destroy();
        });
    }
}