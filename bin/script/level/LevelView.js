function LevelView(game) {
    this.assembleLevelContent = function() {
        that.levelContentGroup.add(that.gridManager.tileSprites);
        that.levelContentGroup.add(that.botManager.botSprites);
    };

    this.fitLevelToView = function() {
        findBestFittingScale();
        that.levelContentGroup.x = (game.camera.width - that.levelContentGroup.width) / 2;
    };

    function findBestFittingScale() {
        var rawHeight = that.gridManager.TileSize * that.gridManager.Tiles.length;
        var rawWidth = that.gridManager.TileSize * that.gridManager.Tiles[0].length;

        that.scale = Math.min(1, that.maxHeight/rawHeight, game.camera.width/rawWidth);
        if (that.scale < 1) {
            that.levelContentGroup.width = rawWidth * that.scale;
            that.levelContentGroup.height = rawHeight * that.scale;
        }
    }

    this.gridManager;
    this.botManager;
    this.levelContentGroup = game.add.group();
    this.maxHeight = 460;
    this.scale = 1;

    var that = this;
}