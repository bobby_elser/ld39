#!/usr/local/bin/python

import sys
import shutil, shlex
import subprocess, os
import datetime, time
import ConfigParser

def copytree(src, dst):
    for item in os.listdir(src):
        s = os.path.join(src, item)
        d = os.path.join(dst, item)
        if os.path.isdir(s):
            shutil.copytree(s, d, False, None)
        else:
            shutil.copy2(s, d)

def searchReplaceInFile(fileName, searchStr, replaceStr):
  fileRead = open(fileName, 'r')
  htmlStr = fileRead.read()
  fileRead.close()
  htmlStr = htmlStr.replace(searchStr, replaceStr)
  fileWrite = open(fileName, 'w')
  fileWrite.write(htmlStr)
  fileWrite.close()

rootDir = os.path.abspath(".")
jsfiles = []
srcDir = os.path.join(rootDir, "proj")
scriptDir = os.path.join(srcDir, "script")
binDir = os.path.join(rootDir, "bin")
outputHtml = os.path.join(binDir, "index.html")

if(os.path.exists(binDir)):
  shutil.rmtree(binDir)

os.makedirs(binDir)
copytree(srcDir, binDir)

for root, dirs, files in os.walk(scriptDir):
  for name in files:
    absoluteFilePath = os.path.join(root,name)
    relativeFilePath = os.path.relpath(absoluteFilePath, srcDir)
    jsfiles.append(relativeFilePath.replace("\\","/"))

searchStr = "<!--##SCRIPTSRC##-->"
replaceStr = ""
for jsfile in jsfiles:
  replaceStr = ("%s        <script src=\"%s\"></script>\n" % (replaceStr, jsfile))
searchReplaceInFile(outputHtml, searchStr, replaceStr)

print( "Build complete!")
