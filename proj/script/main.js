window.addEventListener("load", startGame);

function startGame() {
    var game = new Phaser.Game( 800, 600, Phaser.AUTO, 'ld39-game', { preload: preload, create: create });

    function preload() {
        game.load.json('level0', 'levels/level0.json');
        game.load.json('level1', 'levels/level1.json');
        game.load.json('level2', 'levels/level2.json');
        game.load.json('level3', 'levels/level3.json');
        game.load.json('level4', 'levels/level4.json');
    }

    function create() {
        game.state.add('level0', new LevelState(game, game.cache.getJSON('level0')));
        game.state.add('level1', new LevelState(game, game.cache.getJSON('level1')));
        game.state.add('level2', new LevelState(game, game.cache.getJSON('level2')));
        game.state.add('level3', new LevelState(game, game.cache.getJSON('level3')));
        game.state.add('level4', new LevelState(game, game.cache.getJSON('level4')));
        game.state.start('level0');
    }
}