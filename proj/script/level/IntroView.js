function IntroView(game, levelData) {
    this.present = function() {
        viewGroup.visible = true;
        viewGroup.alpha = 0;
        var tween = game.add.tween(viewGroup).to({alpha: [1, 1, 0]}, 3000);
        tween.onComplete.add(finishHiding);
        tween.start();
    };

    function finishHiding() {
        viewGroup.alpha = 0;
        viewGroup.visible = false;
    }

    var that = this;
    var bitmapDataCollection = [];
    var viewGroup = game.add.group();
    var overlayBMD = Utils.createRectBitmapData(game, game.camera.width, game.camera.height, "black");
    var overlay = viewGroup.create(0,0,overlayBMD);
    var titleTextView = game.add.text(game.camera.width/2, game.camera.height/2, `Level ${levelData.levelNumber}\n"${levelData.levelName}"`, {fill: 'white', fontSize: 64, align: 'center'});
    titleTextView.anchor.x = 0.5;
    titleTextView.anchor.y = 0.5;
    viewGroup.add(titleTextView);

    viewGroup.visible = false;
    viewGroup.alpha = 0;

    overlay.alpha = 0.8;
    overlay.inputEnabled = true;
    bitmapDataCollection.push(overlayBMD);

    this.destroy = function() {
        bitmapDataCollection.forEach((bmd) => {
            bmd.destroy();
        });
    }
}