function Executor() {
    this.startExecutionMode = function() {
        this.endEvaluator.reset();
        this.play();
    }

    this.play = function() {
        if (that.stopped) {
            reset();
            that.stopped = false;
            prepareForStep(0);
            startStep(0);
        }
    };

    this.stop = function() {
        this.stopped = true;
        finishCurrentStep();
        reset();
    }

    this.pause = function() {
        this.stopped = true;
    }

    this.update = function() {
        if (!this.stopped) {
            var now = Date.now();
            var t = (now - stepStart) / this.stepDurationMillis;
            if (t >= 1.0) {
                var nextStep = this.currentStep + 1;
                finishCurrentStep();
                prepareForStep(nextStep);
                startStep(nextStep);
            }
        }
    }

    function finishCurrentStep() {
        that.botManager.bots.forEach((bot) => {
            bot.finishCurrentCommand();
        });
        that.currentStep++;
        if (!that.stopped) {
            that.endEvaluator.evaluateEndConditions();
        }
    }

    function prepareForStep(step) {
        that.botManager.bots.forEach((bot) => {
            var command = bot.commands[bot.getCommandIndexForStep(step)];
            if (bot.getEnergy() === 0) {
                //TODO: countdown to bot death? 
                command = that.commandBuilder.build({type: CommandTypes.Drained}, bot);
                console.log("Bot has no energy.");
            } else if (bot.getEnergy() < command.energyCost) {
                //TODO: default to (special?) "wait" command? Show low energy fault
                command = that.commandBuilder.build({type: CommandTypes.Wait}, bot);
                console.log("Bot has too little energy, waiting instead.");
            }
            bot.currentCommand = command;
            command.prepareToChooseAnimation();
        });
    }

    function startStep(step) {
        that.currentStep = step;
        that.commandSequencer.selectSlotForStep(that.currentStep);
        that.botManager.bots.forEach((bot) => {
            bot.currentCommand.animate();
        });
        stepStart = Date.now();
    }

    function reset() {
        that.currentStep = 0;
        that.resetDelegate();
    }
    
    this.commandBuilder;
    this.commandSequencer;
    this.botManager;
    this.endEvaluator;

    this.playbackSpeed = 1;
    this.stepDurationMillis = 800;
    this.currentStep;
    this.resetDelegate;
    this.stopped = true;
    this.attempts = 0;

    var that = this;
    var stepStart;
}