function BotManager(game, botData) {

    function buildBotBitmapData() {
        var bmd = Utils.createCircleBitmapData(game, botRadius, 'green', 'black', 2);
        botBMDIndex = bitmapDataCollection.push(bmd) - 1;
        return bmd;
    }

    this.init = function() {
        botRadius = that.gridManager.TileSize * 0.35;
        var botBMD = buildBotBitmapData();

        botData.forEach((bot) => {
            var botCenter = that.gridManager.getWorldPositionOfTileCenter(bot.gridPosition.x,bot.gridPosition.y);
            var sprite = game.add.sprite(botCenter.x,botCenter.y,botBMD);
            sprite.anchor.x = 0.5;
            sprite.anchor.y = 0.5;
            sprite.inputEnabled = true;
            sprite.events.onInputDown.add(() => that.onBotSelected(newBot));
            var newBot = new Bot(game, sprite, bot, that.gridManager.TileSize);
            var newCommands = [];
            bot.commands.forEach((commandData) => {
                var newCommand = that.commandBuilder.build(commandData, newBot);
                newCommands.push(newCommand);
            });
            newBot.commands = newCommands;
            that.bots.push(newBot);
            that.botSprites.add(newBot.view);
            bot.realization = newBot;
        });
    }

    this.onBotSelected = function(bot) {
        that.commandSequencer.selectBot(bot);
    };

    this.revertBotsToInitialState = function() {
        this.bots.forEach((bot) => {
            bot.reset();
            var worldPosition = that.gridManager.getWorldPositionOfTileCenter(bot.gridPosition.x, bot.gridPosition.y);
            bot.view.x = worldPosition.x;
            bot.view.y = worldPosition.y;
        });
    };

    this.gridManager;
    this.commandBuilder;
    this.commandSequencer;
    this.bots = [];
    this.botSprites = game.add.group();

    var that = this;
    var bitmapDataCollection = [];
    var botBMDIndex;
    var botRadius;

    this.destroy = function() {
        bitmapDataCollection.forEach((bmd) => {
            bmd.destroy();
        });
    }
}