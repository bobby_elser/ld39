function CommandBuilder(game) {
    this.build = function(commandData, bot) {
        switch (commandData.type) {
            case CommandTypes.Move:
                return new MoveCommand(bot, commandData.x, commandData.y, this.gridManager, game, this.executor, this.botManager);
            case CommandTypes.Drained:
                return new DrainedCommand(bot);
            case CommandTypes.Wait:
            default:
                return new WaitCommand(bot);
        }
    }

    this.gridManager;
    this.executor;
    this.botManager;
}

var CommandTypes = {
    Move: "move",
    Wait: "wait",
    Drained: "drained"
}